----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/05/2021 07:35:18 PM
-- Design Name: 
-- Module Name: video_adjust_unit_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity video_adjust_unit_tb is
end video_adjust_unit_tb;

architecture stimulus of video_adjust_unit_tb is
  constant CLK_PERIOD : time := 10 ns;

  -- DUT signals
  signal red_i   : STD_LOGIC_VECTOR (7 downto 0);
  signal green_i : STD_LOGIC_VECTOR (7 downto 0);
  signal blue_i  : STD_LOGIC_VECTOR (7 downto 0);
  signal hsync_i : STD_LOGIC;
  signal vsync_i : STD_LOGIC;
  signal blank_i : STD_LOGIC;
 
  signal hue_add : STD_LOGIC_VECTOR (9 downto 0); -- signed
  signal sat_add : STD_LOGIC_VECTOR (8 downto 0); -- signed
  signal val_add : STD_LOGIC_VECTOR (8 downto 0); -- signed
 
  signal red_o   : STD_LOGIC_VECTOR (7 downto 0);
  signal green_o : STD_LOGIC_VECTOR (7 downto 0);
  signal blue_o  : STD_LOGIC_VECTOR (7 downto 0);
  signal hsync_o : STD_LOGIC;
  signal vsync_o : STD_LOGIC;
  signal blank_o : STD_LOGIC;
 
  signal clk : STD_LOGIC;
  signal en : STD_LOGIC;
  
  -- Integer shortcuts
  signal red_int :   integer range 0 to 255;
  signal green_int : integer range 0 to 255;
  signal blue_int :  integer range 0 to 255;
  
  signal hue_add_int : integer range -384 to 383;
  signal sat_add_int : integer range -256 to 255;
  signal val_add_int : integer range -256 to 255;
begin

  -- Instanciate DUT
  DUT: entity work.video_adjust_unit
    port map (
      red_i   => red_i,
      green_i => green_i,
      blue_i  => blue_i,
      hsync_i => hsync_i,
      vsync_i => vsync_i,
      blank_i => blank_i,

      hue_add => hue_add,
      sat_add => sat_add,
      val_add => val_add,

      red_o   => red_o,
      green_o => green_o,
      blue_o  => blue_o,
      hsync_o => hsync_o,
      vsync_o => vsync_o,
      blank_o => blank_o,
    
      clk => clk,
      en  => en
    );

  -- Drive clk
  drive_clk : process is
  begin
    clk <= '0';
    wait for CLK_PERIOD / 2;
    clk <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  red_i   <= std_logic_vector(to_unsigned(red_int, red_i'length));
  green_i <= std_logic_vector(to_unsigned(green_int, green_i'length));
  blue_i  <= std_logic_vector(to_unsigned(blue_int, blue_i'length));

  hue_add <= std_logic_vector(to_signed(hue_add_int, hue_add'length));
  sat_add <= std_logic_vector(to_signed(sat_add_int, sat_add'length));
  val_add <= std_logic_vector(to_signed(val_add_int, val_add'length));

  -- Test: bleu->magenta, blanc -> gris
  test_bench: process
  begin
    en <= '0';
    wait for 2*CLK_PERIOD;
    en <= '1';

    -- Bleu -> Magenta
    red_int <= 0;
    green_int <= 0;
    blue_int <= 255;
    hsync_i <= '1';
    vsync_i <= '0';

    hue_add_int <= 64;
    sat_add_int <= 0;
    val_add_int <= 0;
    wait for CLK_PERIOD;    

    -- Blanc -> Gris
    red_int <= 255;
    green_int <= 255;
    blue_int <= 255;
    hsync_i <= '0';
    vsync_i <= '1';

    hue_add_int <= 0;
    sat_add_int <= 0;
    val_add_int <= -128;
    wait for CLK_PERIOD;

    -- Couleur quelconque, correction quelconque et hors-limites
    red_int <= 35;
    green_int <= 41;
    blue_int <= 254;
    hsync_i <= '1';
    vsync_i <= '0';

    hue_add_int <= 64;
    sat_add_int <= -85;
    val_add_int <= 258;
    wait for CLK_PERIOD;

    wait;
  end process test_bench;
end stimulus;
