----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/16/2021 01:35:40 PM
-- Design Name: 
-- Module Name: blink_clk_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity blink_clk_tb is
end blink_clk_tb;

architecture stimulus of blink_clk_tb is
  constant CLK_PERIOD : time := 10ns;
  
  signal clk_in_100MHz : std_logic;
  signal en : std_logic;
  signal clk_out_2500ms : std_logic;
  signal clk_out_1200ms : std_logic;
  signal clk_out_600ms : std_logic;
  signal clk_out_300ms : std_logic;
  signal clk_out_150ms : std_logic;
begin

  DUT : entity work.blink_clk(rtl)
    port map (
      clk_in_100MHz => clk_in_100MHz,
      en => en,
      
      clk_out_2500ms => clk_out_2500ms,
      clk_out_1200ms => clk_out_1200ms,
      clk_out_600ms => clk_out_600ms,
      clk_out_300ms => clk_out_300ms,
      clk_out_150ms => clk_out_150ms
    );
  
  drive_clk : process is
  begin
    clk_in_100MHz <= '0';
    wait for CLK_PERIOD / 2;
    clk_in_100MHz <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  en <= '0', '1' after 2*CLK_PERIOD;
end stimulus;
