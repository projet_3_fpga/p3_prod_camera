----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/12/2021 02:27:01 PM
-- Design Name: 
-- Module Name: i2c_master_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity i2c_master_tb is
end i2c_master_tb;

architecture stimulus of i2c_master_tb is
  constant CLK_PERIOD : time := 10ns; -- 100MHz
  constant SCL_PERIOD : time := 2500ns; -- 400kHz
  
  signal sda_i : STD_LOGIC;
  signal sda_o : STD_LOGIC;
  signal scl   : STD_LOGIC;

  signal i2c_go : STD_LOGIC;
  signal i2c_dn : STD_LOGIC;

  signal slave_addr : STD_LOGIC_VECTOR (6 downto 0);
  signal r_w : STD_LOGIC;

  signal reg_addr : STD_LOGIC_VECTOR (15 downto 0);
  signal data_wr :  STD_LOGIC_VECTOR (7 downto 0);
  signal data_rd :  STD_LOGIC_VECTOR (7 downto 0);

  signal clk_100MHz : STD_LOGIC;
  signal rst : STD_LOGIC;
begin

  -- Instantiate DUT
  DUT: entity work.i2c_master
    port map (
      sda_i => sda_i,
      sda_o => sda_o,
      scl   => scl,

      i2c_go => i2c_go,
      i2c_dn => i2c_dn,

      slave_addr => slave_addr,
      r_w => r_w,

      reg_addr => reg_addr,
      data_wr => data_wr,
      data_rd => data_rd,

      clk_100MHz => clk_100MHz,
      rst => rst
    );

  -- Drive clk
  drive_clk : process is
  begin
    clk_100MHz <= '0';
    wait for CLK_PERIOD / 2;
    clk_100MHz <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  -- Reset
  rst <= '1', '0' after 2*CLK_PERIOD;
  
  test_bench: process is
  begin
    i2c_go <= '0';
    sda_i <= '1';

    -- Set slave address, register, data to be read
    report "Device: 0x3C; Register: 0x300A; Expected read: 0x56";
    slave_addr <= "0111100"; -- 0x3C
    r_w <= '1';
    reg_addr <= x"300A";
    wait for 2*SCL_PERIOD;

    -- Give it a go
    i2c_go <= '1';
    wait for SCL_PERIOD;
    i2c_go <= '0';

    -- Control SDA
    wait for 26*SCL_PERIOD;
    sda_i <= '0';
    wait for SCL_PERIOD;
    sda_i <= '1';
    wait for SCL_PERIOD;
    sda_i <= '0';
    wait for SCL_PERIOD;
    sda_i <= '1';
    
    wait for SCL_PERIOD;
    sda_i <= '0';
    wait for SCL_PERIOD;
    sda_i <= '1';
    wait for SCL_PERIOD;
    sda_i <= '1';
    wait for SCL_PERIOD;
    sda_i <= '0';
    wait for SCL_PERIOD;
    
    -- Wait until done
    sda_i <= '1'; -- slave deasserts SDA
    wait until (i2c_dn = '1') for 40*SCL_PERIOD;

    -- Check data read against 0x56

    -- Set slave address, write, register, data to be written
    report "Device: 0x4C; Register: 0x3103; Write: 0x11";
    wait for 7*SCL_PERIOD;
    slave_addr <= "1001100"; -- 0x4C
    r_w <= '0';
    reg_addr <= x"3103";
    data_wr <= x"11";
    wait for SCL_PERIOD;

    -- Give it a go
    i2c_go <= '1';
    wait for SCL_PERIOD;
    i2c_go <= '0';

    -- Wait until done
    wait until (i2c_dn = '1') for 40*SCL_PERIOD;
    wait;

  end process test_bench;

end stimulus;
