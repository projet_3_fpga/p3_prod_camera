----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/20/2021 05:21:55 PM
-- Design Name: 
-- Module Name: menu_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity menu_tb is
end menu_tb;

architecture stimulus of menu_tb is
  constant CLK_PERIOD : time := 10ns;

  -- Entrées
  signal ds_en : std_logic_vector(3 downto 0);
  signal seven_segment : std_logic_vector(7 downto 0);
  signal buttons : std_logic_vector(3 downto 0);

  -- Sorties
  signal reg_hue : std_logic_vector(9 downto 0);
  signal reg_sat : std_logic_vector(9 downto 0);
  signal reg_val : std_logic_vector(9 downto 0);

  -- Misc.
  signal en : std_logic;
  signal clk : std_logic;

  -- Constantes relatives aux boutons
  alias BTNL is buttons(0);
  alias BTND is buttons(1);
  alias BTNR is buttons(2);
  alias BTNU is buttons(3);
  constant BTNS_ALL_RELEASED : std_logic_vector(3 downto 0) := "0000";

begin

  DUT: entity work.menu(rtl)
    port map (
      ds_en => ds_en,
      seven_segment => seven_segment,
      buttons => buttons,

      reg_hue => reg_hue,
      reg_sat => reg_sat,
      reg_val => reg_val,

      en => en,
      clk => clk
   );

  drive_clk : process is
  begin
    clk <= '0';
    wait for CLK_PERIOD / 2;
    clk <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  en <= '0', '1' after 2*CLK_PERIOD;

  test_bench: process is
  begin
    -- State: start
    buttons <= BTNS_ALL_RELEASED;
    wait for 4*CLK_PERIOD;

    -- State: select_hue
    BTND <= '1';
    wait for CLK_PERIOD;

    -- State: wait_release_select_sat
    wait for 3*CLK_PERIOD;
    BTND <= '0';
    wait for 2*CLK_PERIOD;

    -- State: select_sat
    BTND <= '1';
    wait for CLK_PERIOD;

    -- State: wait_release_select_val
    wait for 3*CLK_PERIOD;
    BTND <= '0';
    wait for 2*CLK_PERIOD;

    -- State: select_val
    BTNU <= '1';
    wait for CLK_PERIOD;

    -- State: wait_release_select_sat
    wait for 3*CLK_PERIOD;
    BTNU <= '0';
    wait for 2*CLK_PERIOD;

    -- State: select_sat
    BTNR <= '1';
    wait for CLK_PERIOD;

    -- State: wait_release_set_reg
    wait for 3*CLK_PERIOD;
    BTNR <= '0';
    wait for 2*CLK_PERIOD;

    -- State: set_reg
    BTND <= '1';
    wait for CLK_PERIOD;

    --State: decr_sat
    wait for CLK_PERIOD;

    --State: wait_release_set_reg
    wait for 3*CLK_PERIOD;
    BTND <= '0';
    wait for 2*CLK_PERIOD;

    --State: set_reg
    BTNU <= '1';
    wait for CLK_PERIOD;

    --State: incr_sat
    wait for CLK_PERIOD;

    --State: wait_release_set_reg
    BTNU <= '0';
    wait for CLK_PERIOD;

    --State: set_sat
    BTNU <= '1';
    wait for CLK_PERIOD;

    --State: incr_sat
    wait for CLK_PERIOD;

    --State: wait_release_set_reg
    BTNU <= '0';
    wait for CLK_PERIOD;

    --State: set_reg
    BTNR <= '1';
    wait for CLK_PERIOD;

    --State: wait_release_confirm
    wait for 3*CLK_PERIOD;
    BTNR <= '0';
    wait;

  end process test_bench;

end stimulus;
