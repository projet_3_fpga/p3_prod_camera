----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/11/2021 11:32:59 AM
-- Design Name: 
-- Module Name: ov7670_rgb565_decoder_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ov7670_rgb565_decoder_tb is
end ov7670_rgb565_decoder_tb;

architecture stimulus of ov7670_rgb565_decoder_tb is
  constant CLK_PERIOD : time := 10ns; -- 25MHz period : 400μs

  -- DUT signals
  signal vsync        : STD_LOGIC;
  signal hsync        : STD_LOGIC;
  signal pclk         : STD_LOGIC;
  signal data         : STD_LOGIC_VECTOR (7 downto 0);

  signal video_pclk   : STD_LOGIC;
  signal video_red    : STD_LOGIC_VECTOR (7 downto 0);
  signal video_green  : STD_LOGIC_VECTOR (7 downto 0);
  signal video_blue   : STD_LOGIC_VECTOR (7 downto 0);
  signal video_hsync  : STD_LOGIC;
  signal video_vsync  : STD_LOGIC;
  signal video_blank  : STD_LOGIC;
  
  signal rst : STD_LOGIC;
begin

  -- Drive pclk
  drive_clk : process is
  begin
    pclk <= '0';
    wait for CLK_PERIOD / 2;
    pclk <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  -- Instanciate DUT
  DUT: entity work.ov7670_rgb565_decoder
    port map ( 
      vsync        => vsync,
      hsync        => hsync,
      pclk         => pclk,
      data         => data,

      video_pclk   => video_pclk,
      video_red    => video_red,
      video_green  => video_green,
      video_blue   => video_blue,
      video_hsync  => video_hsync,
      video_vsync  => video_vsync,
      video_blank  => video_blank,
      
      rst => rst
    );

  -- TB: clock in R, G, B, watch for color and pclk outputs
  test_bench: process is
  begin
    rst <= '1';
    wait for 2*CLK_PERIOD;
    rst <= '0';

    hsync <= '1';
    vsync <= '0';

    -- Rouge
    data <= "11111000";
    wait for CLK_PERIOD;

    data <= "00000000";
    wait for CLK_PERIOD;

    -- Vert
    data <= "00000111";
    wait for CLK_PERIOD;

    data <= "11100000";
    wait for CLK_PERIOD;

    -- Bleu
    data <= "00000000";
    wait for CLK_PERIOD;

    data <= "00011111";
    wait for CLK_PERIOD;

    -- HSYNC
    hsync <= '0';
    data <= "00000000";
    wait for CLK_PERIOD;

    data <= "00000000";
    wait for CLK_PERIOD;

    -- HSYNC
    hsync <= '0';
    data <= "00000000";
    wait for 2*CLK_PERIOD;

    -- VSYNC
    hsync <= '1';
    vsync <= '1';
    data <= "00000000";
    wait for 2*CLK_PERIOD;

    wait;
  end process test_bench;
end stimulus;
