----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/16/2021 03:10:15 PM
-- Design Name: 
-- Module Name: str_to_7seg_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity str_to_7seg_tb is
end str_to_7seg_tb;

architecture stimulus of str_to_7seg_tb is
  constant CLK_PERIOD : time := 10ns;
  
  signal clk : std_logic;
  signal en : std_logic;
  signal addr : std_logic_vector(9 downto 0);
  signal data : std_logic_vector(31 downto 0);
begin
  DUT: entity work.str_to_7seg(rtl)
    port map (
       clk  => clk,
       en   => en,
       addr => addr, -- string code
       data => data-- segments print
    );  

  drive_clk : process is
  begin
    clk <= '0';
    wait for CLK_PERIOD / 2;
    clk <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  test_bench: process is
  variable i : integer := 0;
  begin
    addr <= (others => '0');
    wait for 3*CLK_PERIOD;
    for i in 785 downto 0 loop
      addr <= std_logic_vector(to_unsigned(i, addr'length));
      wait for CLK_PERIOD;
    end loop;
  end process test_bench;

  en <= '0', '1' after 2*CLK_PERIOD;

end stimulus;
