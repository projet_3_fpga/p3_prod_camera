----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/05/2021 01:51:50 PM
-- Design Name: 
-- Module Name: delay_vector_tb - stimulus
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity delay_vector_tb is
--  Port ( );
end delay_vector_tb;

architecture stimulus of delay_vector_tb is
  constant CLK_PERIOD : time := 10 ns;
  
  -- Define DUT signal copies
  signal input   : std_logic_vector(3 downto 0);
  signal delayed : std_logic_vector(3 downto 0);

  signal clk : STD_LOGIC;
  signal rst : STD_LOGIC;
begin
  
  -- Instantiate DUT
  DUT: entity work.delay_vector
    generic map (
      CLK_FREQUENCY_HZ => 100_000_000, -- 100 MHz
      DELAY_NS => 100, -- 0.1μs
      WIDTH => 4
    )
    port map (
      input   => input,
      delayed => delayed,
  
      clk => clk,
      rst => rst
    );
    
  -- Rst
  rst <= '1', '0' after 2*CLK_PERIOD;

  -- Drive clock
  drive_clk : process is
  begin
    clk <= '0';
    wait for CLK_PERIOD / 2;
    clk <= '1';
    wait for CLK_PERIOD / 2;
  end process drive_clk;

  test_bench: process is
  begin
    input <= "0000";
    wait for 2*CLK_PERIOD;
    
    input <= "0001";
    wait for CLK_PERIOD;

    input <= "0010";
    wait for CLK_PERIOD;
    
    input <= "0011";
    wait for CLK_PERIOD;
    
    input <= "0100";
    wait for CLK_PERIOD;
    
    input <= "0101";
    wait for 4*CLK_PERIOD;

    input <= "0110";
    wait for CLK_PERIOD;

    input <= "0000";
    wait;
    -- Check that they come out with the precribed delay
  end process test_bench;   

end stimulus;
