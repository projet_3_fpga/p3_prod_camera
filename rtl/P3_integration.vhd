----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/08/2021 02:05:29 PM
-- Design Name: 
-- Module Name: P3_integration_no_camera - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity P3_integration is
  port (
    -- DEBUG
    debug_leds : out std_logic_vector(7 downto 0);
    
    -- Buttons; debounced, non-inverted logic (1 = pressed)
    menu_btns : in std_logic_vector(3 downto 0);  -- Ordre: 3 downto 0 => Up Down Right Left

    -- 7-segments, non-inverted logic (1 = lit, enabled)
    seven_segment   : out std_logic_vector(7 downto 0); -- Ordre: 7 downto 0 => ABCDEFG<DP>
    seven_seg_ds_en : out std_logic_vector(3 downto 0); -- écran gauche: enable(0)
            
    -- DVI diff pairs
    signal red_s   : out std_logic;
    signal green_s : out std_logic;
    signal blue_s  : out std_logic;
    signal clock_s : out std_logic;
    
    -- OV7670 camera
    signal cam_scl   : out std_logic;
    signal cam_sda_i : in  std_logic;
    signal cam_sda_o : out std_logic;
    signal cam_vsync : in  std_logic;
    signal cam_hsync : in  std_logic;
    signal cam_pclk  : in  std_logic;
    signal cam_xclk  : out std_logic;
    signal cam_data  : in  std_logic_vector(7 downto 0);
    signal cam_rst   : out std_logic;
    signal cam_pwdn  : out std_logic;

    -- Clocking
    signal dvi_clk     : in STD_LOGIC;
    signal dvi_clk_n   : in STD_LOGIC;
    signal clk_100MHz  : in STD_LOGIC;
    signal ov7670_xclk : in STD_LOGIC;
    signal clk_locked  : in STD_LOGIC;
    signal dvi_clk_locked : in STD_LOGIC;
    
    signal video_pclk     : out STD_LOGIC;
    signal video_pclk_pll : in  std_logic
  );
end P3_integration;

architecture structural of P3_integration is

  -- VGA outputs
  signal pclk       : STD_LOGIC;
  signal red        : STD_LOGIC_VECTOR (7 downto 0);
  signal green      : STD_LOGIC_VECTOR (7 downto 0);
  signal blue       : STD_LOGIC_VECTOR (7 downto 0);
  signal hsync      : STD_LOGIC;
  signal vsync      : STD_LOGIC;
  signal blank      : STD_LOGIC;

  -- Adjustment registers
  signal hue_add : STD_LOGIC_VECTOR (9 downto 0); -- signed
  signal sat_add : STD_LOGIC_VECTOR (8 downto 0); -- signed
  signal val_add : STD_LOGIC_VECTOR (8 downto 0); -- signed

  -- HSV2RGB outputs
  signal red_adjusted   : STD_LOGIC_VECTOR (7 downto 0);
  signal green_adjusted : STD_LOGIC_VECTOR (7 downto 0);
  signal blue_adjusted  : STD_LOGIC_VECTOR (7 downto 0);

  -- Delay outputs
  signal hSync_delayed : STD_LOGIC;
  signal vSync_delayed : STD_LOGIC;
  signal blank_delayed : STD_LOGIC;

  -- Menu
  signal menu_reg_hue : std_logic_vector(9 downto 0);
  signal menu_reg_sat : std_logic_vector(8 downto 0);
  signal menu_reg_val : std_logic_vector(8 downto 0);

  signal rst : std_logic;
begin
  -- DEBUG
  debug_leds(7 downto 3) <= red_adjusted(7 downto 3);
  debug_leds(2)          <= '1';
  debug_leds(1)          <= video_pclk_pll;
  debug_leds(0)          <= dvi_clk_locked;

  rst <= not clk_locked;

  inst_ov7670_driver : entity work.ov7670_driver
    port map (
      -- OV7670 camera
      scl   => cam_scl,
      sda_i => cam_sda_i,
      sda_o => cam_sda_o,
      vsync => cam_vsync,
      hsync => cam_hsync,
      pclk  => cam_pclk,
      xclk  => cam_xclk,
      data  => cam_data,
      rst   => cam_rst,
      pwdn  => cam_pwdn,
      
      -- Native video out
      video_pclk  => pclk,
      video_red   => red,
      video_green => green,
      video_blue  => blue,
      video_hsync => hsync,
      video_vsync => vsync,
      video_blank => blank,
  
      -- Supplied XCLK is passed-through to output
      xclk_in => ov7670_xclk,
      
      driver_rst => rst
    );

  video_pclk <= pclk; -- send PCLK out to a clk_wizard that will generate the dvi clocks from it

  -- Instanciate adjust unit
  inst_video_adjust_unit: entity work.video_adjust_unit
    port map (
      red_i   => red,
      green_i => green,
      blue_i  => blue,
      hsync_i => hsync,
      vsync_i => vsync,
      blank_i => blank,

      hue_add => menu_reg_hue,
      sat_add => menu_reg_sat,
      val_add => menu_reg_val,

      red_o   => red_adjusted,
      green_o => green_adjusted,
      blue_o  => blue_adjusted,
      hsync_o => hsync_delayed,
      vsync_o => vsync_delayed,
      blank_o => blank_delayed,
    
      clk => video_pclk_pll,
      en  => dvi_clk_locked
    );

  -- Instanciatiate menu
  -- TODO: instanciate menu
  inst_menu: entity work.menu
    port map (
      ds_en         => seven_seg_ds_en,      
      seven_segment => seven_segment,
      buttons       => menu_btns,

      reg_hue       => menu_reg_hue,
      reg_sat       => menu_reg_sat,
      reg_val       => menu_reg_val,

      en            => dvi_clk_locked,
      clk           => video_pclk_pll
    );

  -- Instanciate DVI out
  inst_dvi: entity work.dvid
    port map (
      clk       => dvi_clk,
      clk_n     => dvi_clk_n,
      clk_pixel => video_pclk_pll,

      red_p     => red_adjusted,
      green_p   => green_adjusted,
      blue_p    => blue_adjusted,
      blank     => blank_delayed,
      hsync     => hsync_delayed,
      vsync     => vsync_delayed,

      red_s     => red_s,
      green_s   => green_s,
      blue_s    => blue_s,
      clock_s   => clock_s
    );


end structural;
