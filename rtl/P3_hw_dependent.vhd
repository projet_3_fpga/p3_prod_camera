----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/08/2021 04:09:55 PM
-- Design Name: 
-- Module Name: P3_hw_dependent - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity P3_hw_dependent is
    port (
      -- DEBUG
      debug_leds : out std_logic_vector(7 downto 0);
      
      btns_hw : in STD_LOGIC_VECTOR (3 downto 0);
      seven_segment_hw : out STD_LOGIC_VECTOR (7 downto 0);
      seven_seg_ds_en_hw : out STD_LOGIC_VECTOR (3 downto 0);

      cam_scl   : out   std_logic;
      cam_sda   : inout std_logic;
      cam_vsync : in    std_logic;
      cam_hsync : in    std_logic;
      cam_pclk  : in    std_logic;
      cam_xclk  : out   std_logic;
      cam_data  : in    std_logic_vector(7 downto 0);
      cam_rst   : out   std_logic;
      cam_pwdn  : out   std_logic;

      red_s : out STD_LOGIC;
      green_s : out STD_LOGIC;
      blue_s : out STD_LOGIC;
      clock_s : out STD_LOGIC;

      rst_btn : in STD_LOGIC;
      crystal_clk : in STD_LOGIC
    );
end P3_hw_dependent;

architecture structural of P3_hw_dependent is
  -- Signals
  
  -- OV7670
  signal cam_scl_no_oc : std_logic;
  signal cam_sda_i : std_logic;
  signal cam_sda_o : std_logic;
  
  -- Clocking wizard
  signal dvi_clk     : std_logic;
  signal dvi_clk_n   : std_logic;
  signal pclk        : std_logic;
  signal clk_100MHz  : std_logic;
  signal ov7670_xclk : std_logic;
  signal clk_locked  : std_logic;
  
  component clk_wiz_0
    port (
      -- Clock out ports  
      signal clk_100MHz  : out std_logic;
      signal ov7670_xclk : out std_logic;
      signal locked      : out std_logic;
 
      -- Clock in ports
      signal clk_in : in std_logic;
      signal reset : in std_logic
    );
  end component;

  component clk_wiz_1
    port (
      -- Clock out ports  
      signal dvi_clk        : out std_logic;
      signal dvi_clk_n      : out std_logic;
      signal video_pclk_pll : out std_logic;
      signal locked         : out std_logic;
 
      -- Clock in ports
      signal video_pclk : in std_logic;
      signal reset : in std_logic
    );
  end component;

  signal video_pclk     : std_logic;
  signal video_pclk_pll : std_logic;
  signal dvi_clk_locked : std_logic;

  -- Hardware BSP
  signal btns            : std_logic_vector(3 downto 0);
  signal seven_segment   : std_logic_vector(7 downto 0);
  signal seven_seg_ds_en : std_logic_vector(3 downto 0);

begin

  -- GPIO HW logic adjustment
  mimas_a7v3_hw_bsp_inst: entity work.mimas_a7v3_hw_bsp
    port map (
      btns_hw            => btns_hw,
      btns               => btns,
      seven_segment_hw   => seven_segment_hw,
      seven_segment      => seven_segment,
      seven_seg_ds_en_hw => seven_seg_ds_en_hw,
      seven_seg_ds_en    => seven_seg_ds_en,
      
      clk_100MHz => clk_100MHz
    );

  -- Instanciate clock wizard
  inst_mmcm : clk_wiz_0
    port map ( 
      -- Clock out ports  
      clk_100MHz  => clk_100MHz,
      ov7670_xclk => ov7670_xclk,
      locked      => clk_locked,
      
      -- Clock in ports
      clk_in => crystal_clk,
      reset => rst_btn
    );

  -- Instanciate DVI clock wizard
  inst_mmcm_dvi : clk_wiz_1
    port map ( 
      -- Clock out ports  
      dvi_clk     => dvi_clk,
      dvi_clk_n   => dvi_clk_n,
      video_pclk_pll => video_pclk_pll,
      locked      => dvi_clk_locked,
      
      -- Clock in ports
      video_pclk => ov7670_xclk, -- DEBUG video_pclk,
      reset => rst_btn
    );

  -- Broches SCL & SDA open-drain
  i2c_master_o_c_inst: entity work.i2c_master_o_c
    port map (
      scl   => cam_scl_no_oc,
      sda_i => cam_sda_i,
      sda_o => cam_sda_o,

      scl_o_c => cam_scl,
      sda_o_c => cam_sda
    );
  
  -- HW-independent block
  P3_integration_inst: entity work.P3_integration
    port map (
      -- DEBUG
      debug_leds => debug_leds,

      -- Buttons; debounced, non-inverted logic (1 = pressed)
      menu_btns => btns,
  
      -- 7-segments, non-inverted logic (1 = lit, enabled)
      seven_segment   => seven_segment,
      seven_seg_ds_en => seven_seg_ds_en,

      -- DVI diff pairs
      red_s   => red_s,
      green_s => green_s,
      blue_s  => blue_s,
      clock_s => clock_s,
      
      -- OV7670 camera
      cam_scl   => cam_scl_no_oc,
      cam_sda_i => cam_sda_i,
      cam_sda_o => cam_sda_o,
      cam_vsync => cam_vsync,
      cam_hsync => cam_hsync,
      cam_pclk  => cam_pclk,
      cam_xclk  => cam_xclk,
      cam_data  => cam_data,
      cam_rst   => cam_rst,
      cam_pwdn  => cam_pwdn,
  
      -- Clocking
      dvi_clk        => dvi_clk,
      dvi_clk_n      => dvi_clk_n,
      clk_100MHz     => clk_100MHz,
      ov7670_xclk    => ov7670_xclk,
      clk_locked     => clk_locked,
      dvi_clk_locked => dvi_clk_locked,

      video_pclk_pll => video_pclk_pll,
      video_pclk     => video_pclk
    );

end structural;
