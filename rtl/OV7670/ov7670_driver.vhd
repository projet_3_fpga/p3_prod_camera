----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/09/2021 01:26:31 PM
-- Design Name: 
-- Module Name: ov7670_driver - structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ov7670_driver is
  port (
    -- OV7670 camera
    scl   : out std_logic;
    sda_i : in  std_logic;
    sda_o : out std_logic;
    vsync : in  std_logic;
    hsync : in  std_logic;
    pclk  : in  std_logic;
    xclk  : out std_logic;
    data  : in  std_logic_vector(7 downto 0);
    rst   : out std_logic;
    pwdn  : out std_logic;
    
    -- Native video out
    video_pclk  : out STD_LOGIC;
    video_red   : out STD_LOGIC_VECTOR (7 downto 0);
    video_green : out STD_LOGIC_VECTOR (7 downto 0);
    video_blue  : out STD_LOGIC_VECTOR (7 downto 0);
    video_hsync : out STD_LOGIC;
    video_vsync : out STD_LOGIC;
    video_blank : out STD_LOGIC;

    -- Supplied XCLK is passed-through to output
    xclk_in : in std_logic; -- around 24MHz
    
    driver_rst : in std_logic
  );
end ov7670_driver;

architecture structural of ov7670_driver is
  
begin

  -- Instanciate OV7670 init
  -- TODO: init camera from FPGA instead of MCU
  -- DEBUG
  scl   <= '1'; -- deassert
  --cam_sda_i 
  sda_o <= '1'; -- deassert
  rst   <= 'Z';
  pwdn  <= 'Z';

  -- Instanciate ov7670_rgb565_decoder interface
  inst_ov7670_rgb565_decoder : entity work.ov7670_rgb565_decoder
    port map ( 
      vsync        => vsync,
      hsync        => hsync,
      pclk         => pclk,
      data         => data,

      video_pclk   => video_pclk,
      video_red    => video_red,
      video_green  => video_green,
      video_blue   => video_blue,
      video_hsync  => video_hsync,
      video_vsync  => video_vsync,
      video_blank  => video_blank,
      
      rst => driver_rst
    );
  
  -- Pass-through XCLK
  xclk <= xclk_in;

end structural;
