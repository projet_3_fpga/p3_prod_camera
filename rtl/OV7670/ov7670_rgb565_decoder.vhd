----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/11/2021 11:03:28 AM
-- Design Name: 
-- Module Name: ov7670_rgb565_decoder - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ov7670_rgb565_decoder is
    port ( 
      vsync        : in STD_LOGIC;
      hsync        : in STD_LOGIC;
      pclk         : in STD_LOGIC;
      data         : in STD_LOGIC_VECTOR (7 downto 0);

      video_pclk   : out STD_LOGIC;
      video_red    : out STD_LOGIC_VECTOR (7 downto 0);
      video_green  : out STD_LOGIC_VECTOR (7 downto 0);
      video_blue   : out STD_LOGIC_VECTOR (7 downto 0);
      video_hsync  : out STD_LOGIC;
      video_vsync  : out STD_LOGIC;
      video_blank  : out STD_LOGIC;
      
      rst : in STD_LOGIC
    );
end ov7670_rgb565_decoder;

architecture rtl of ov7670_rgb565_decoder is
  -- Divers registres
  signal pixel_byte_low  : std_logic_vector (7 downto 0);
  signal pixel_byte_high : std_logic_vector (7 downto 0);
  
  signal pixel_rgb565 : std_logic_vector (15 downto 0);
  
  signal pclk_1 : std_logic;

begin

  pixel_pipeline: process (pclk, rst) is
  begin
    if rising_edge(pclk) then
      if (rst = '0') then
        -- Clock in data dans un registre 8-bits
        pixel_byte_low <= data;

        -- Clock in le 1er registre 8-bits dans un second
        pixel_byte_high <= pixel_byte_low;
        
      else
        pixel_byte_low  <= (others => '0');
        pixel_byte_high <= (others => '0');
      end if;
    end if;
  end process pixel_pipeline;

  -- PCLK: un registre pour créer un délai, puis diviser par 2
  pclk_pipeline: process (pclk, rst) is 
  begin
    if rising_edge(pclk) then
      if (rst = '0') then
        pclk_1 <= not pclk_1;
        video_pclk <= pclk_1;
      else
        pclk_1 <= '1';
        video_pclk <= '1';
      end if;
    end if;
  end process pclk_pipeline;

  -- Clock out an always-valid pixel
  stable_px: process (video_pclk, rst) is
  begin
    if falling_edge(video_pclk) then -- FIXME: Ça me parait pas être une super idée mais ça marche
      if (rst = '0') then
        pixel_rgb565 <= pixel_byte_high & pixel_byte_low;
      else
        pixel_rgb565 <= (others => '0');    
      end if;
    end if;
  end process stable_px;

  -- Pad des 0 à droite du RGB565 pour générer la sortie
  video_red   <= pixel_rgb565(15 downto 11) & "000";
  video_green <= pixel_rgb565(10 downto 5)  & "00";
  video_blue  <= pixel_rgb565(4 downto 0)   & "000";

  -- Délai de trois cycles pour HSYNC et VSYNC
  delay_hsync_vsync: entity work.delay_vector_cycles
    generic map (
      DELAY_CYCLES => 1,
      WIDTH => 2
    )
    port map (
      input(0) => hsync,
      input(1) => vsync,

      delayed(0) => video_hsync,
      delayed(1) => video_vsync,

      clk => pclk,
      rst => rst
    );

  -- Générer blank
  video_blank <= (not video_hsync) or video_vsync;

end rtl;
