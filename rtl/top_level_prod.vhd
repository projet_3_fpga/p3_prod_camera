----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/01/2021 06:06:02 PM
-- Design Name: 
-- Module Name: top_level - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity top_level_prod is
  port (
    -- DEBUG
    LED : out std_logic_vector(7 downto 0);

    enable        : out std_logic_vector(3 downto 0); -- écran gauche: enable(0)
    seven_segment : out std_logic_vector(7 downto 0); -- Ordre: 7 downto 0 => EDC<DP>BAFG
    sw_in         : in std_logic_vector(3 downto 0);  -- Ordre: 3 downto 0 => Up Down Right Left

    hdmi_tx_p      : out  STD_LOGIC_VECTOR(2 downto 0);
    hdmi_tx_n      : out  STD_LOGIC_VECTOR(2 downto 0);
    hdmi_tx_clk_p  : out  std_logic;
    hdmi_tx_clk_n  : out  std_logic;
    
    -- OV7670 camera
    P13_0  : out  std_logic; -- SCL
    P13_2  : inout  std_logic; -- SDA
    P13_4  : in   std_logic;    -- VSYNC
    P13_6  : in   std_logic;    -- HSYNC
    P13_8  : in   std_logic;    -- PCLK
    P13_10 : out  std_logic;    -- XCLK
    P13_12 : in   std_logic;    -- D7
    P13_14 : in   std_logic;    -- D6
    P13_16 : in   std_logic;    -- D5
    P13_18 : in   std_logic;    -- D4
    P13_20 : in   std_logic;    -- D3
    P13_22 : in   std_logic;    -- D2
    P13_24 : in   std_logic;    -- D1
    P13_26 : in   std_logic;    -- D0
    P13_28 : out  std_logic;   -- RST
    P13_30 : out  std_logic;   -- PWDN

    CLK1 : in std_logic;
    RESET : in std_logic
  );
end top_level_prod;

architecture rtl of top_level_prod is

  -- OV7670
  signal cam_scl   : std_logic;
  signal cam_sda   : std_logic;
  signal cam_vsync : std_logic;
  signal cam_hsync : std_logic;
  signal cam_pclk  : std_logic;
  signal cam_xclk  : std_logic;
  signal cam_data  : std_logic_vector(7 downto 0);
  signal cam_rst   : std_logic;
  signal cam_pwdn  : std_logic;

  -- DVI diff pairs
  signal red_s   : std_logic;
  signal green_s : std_logic;
  signal blue_s  : std_logic;
  signal clock_s : std_logic;

  signal crystal_clk : std_logic;
begin

  -- Caméra
  P13_0       <= cam_scl;
  cam_sda     <= P13_2;
  cam_vsync   <= P13_4;
  cam_hsync   <= P13_6;

  cam_data(7) <= P13_12;
  cam_data(6) <= P13_14;
  cam_data(5) <= P13_16;
  cam_data(4) <= P13_18;
  cam_data(3) <= P13_20;
  cam_data(2) <= P13_22;
  cam_data(1) <= P13_24;
  cam_data(0) <= P13_26;

  P13_28 <= cam_rst;
  P13_30 <= cam_pwdn;

  -- Distribution d'horloge
  BUFG_crystal_inst : BUFG
    port map (
       O => crystal_clk,
       I => CLK1
    );

  BUFG_PCLK_inst : BUFG
    port map (
       O => cam_pclk,
       I => P13_8
    );
    
  BUFG_XCLK_inst : BUFG
    port map (
       O => P13_10,
       I => cam_xclk
    );

  inst_P3_hw_dependent: entity work.P3_hw_dependent
    port map (
      -- DEBUG
      debug_leds => led,

      btns_hw            => sw_in,
      seven_segment_hw   => seven_segment,
      seven_seg_ds_en_hw => enable,

      cam_scl   => cam_scl,
      cam_sda   => cam_sda,
      cam_vsync => cam_vsync,
      cam_hsync => cam_hsync,
      cam_pclk  => cam_pclk,
      cam_xclk  => cam_xclk,
      cam_data  => cam_data,
      cam_rst   => cam_rst,
      cam_pwdn  => cam_pwdn,

      red_s              => red_s,
      green_s            => green_s,
      blue_s             => blue_s,
      clock_s            => clock_s,

      rst_btn            => RESET,
      crystal_clk        => crystal_clk
    );

OBUFDS_red    : OBUFDS port map ( O  => hdmi_tx_p(2), OB => hdmi_tx_n(2), I  => red_s   );
OBUFDS_green  : OBUFDS port map ( O  => hdmi_tx_p(1), OB => hdmi_tx_n(1), I  => green_s );
OBUFDS_blue   : OBUFDS port map ( O  => hdmi_tx_p(0), OB => hdmi_tx_n(0), I  => blue_s  );
OBUFDS_clock  : OBUFDS port map ( O  => hdmi_tx_clk_p, OB => hdmi_tx_clk_n, I  => clock_s );

end rtl;
