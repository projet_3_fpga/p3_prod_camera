----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 06/10/2021 09:34:48 PM
-- Design Name:
-- Module Name: menu - rtl
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity menu is
  port (
    ds_en : out std_logic_vector(3 downto 0);
    seven_segment : out std_logic_vector(7 downto 0); -- Ordre: 7 downto 0 => ABCDEFG<DP>
    buttons : in std_logic_vector(3 downto 0); -- debounced

    reg_hue : out std_logic_vector(9 downto 0);
    reg_sat : out std_logic_vector(8 downto 0);
    reg_val : out std_logic_vector(8 downto 0);

    en : in std_logic;
    clk : in std_logic
  );
end menu;

architecture rtl of menu is
  -- Registres de configuration
  signal reg_select : std_logic_vector(2 downto 0);
  signal reg_wr : std_logic;
  signal reg_being_written : std_logic_vector(9 downto 0);

  -- Timer
  signal timer_en : std_logic;
  signal timer_dn : std_logic;

  -- 7-segments
  signal disp : std_logic_vector(9 downto 0);
  signal segments_parallel : std_logic_vector(31 downto 0);

  signal seven_seg_en : std_logic;
  signal blink_en : std_logic;
  signal blink : std_logic; -- signal clignotant

  signal rst : std_logic;

  component fit_timer_0
    port (
      clk : in std_logic;
      rst : in std_logic;
      interrupt : out std_logic
    );
  end component;

begin

    rst <= not en;

    with blink_en select seven_seg_en <=
      '1' when '0',
      blink when others;

  ---TODO virer architecture
    menu_msa_inst : entity work.menu_msa
      port map (
        -- Entrées
        buttons => buttons,
        reg_hue => reg_hue,
        reg_sat => reg_sat,
        reg_val => reg_val,
        timer_dn => timer_dn,

        -- Sorties
        disp => disp,
        reg_select => reg_select,
        reg_wr => reg_wr,
        reg => reg_being_written,

        blink_en => blink_en,
        timer_en => timer_en,

        -- Misc.
        rst => rst,
        clk => clk
      );

    config_reg_inst : entity work.config_reg(rtl)
      port map (
        reg_select => reg_select,
        reg_wr => reg_wr,
        reg => reg_being_written,

        reg_hue => reg_hue,
        reg_sat => reg_sat,
        reg_val => reg_val,

        rst => rst,
        clk => clk
      );

    str_to_7seg_inst : entity work.str_to_7seg(rtl)
      port map (
        clk => clk,
        en => en,
        addr => disp,
        data => segments_parallel
      );

    P3_7seg_driver_inst : entity work.P3_7seg_driver(rtl)
      port map (
        print_data => segments_parallel,
        segments => seven_segment,
        ds_en => ds_en,

        en => seven_seg_en,
        clk => clk
      );

    blink_clk_inst : entity work.blink_clk(rtl)
      port map (
        clk_in_100MHz => clk,
        en => blink_en,

        clk_out_2500ms => open,
        clk_out_1200ms=> open,
        clk_out_600ms => open,
        clk_out_300ms => open,
        clk_out_150ms => blink
      );

    timer_inst : fit_timer_0
      port map (
        clk => clk,
        rst => not timer_en,
        interrupt => timer_dn
      );

end rtl;
