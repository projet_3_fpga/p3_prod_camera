----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/09/2021 05:55:19 PM
-- Design Name: 
-- Module Name: P3_7seg_clk - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments: A simple x1000 clock divider. Generate the clock used
-- to switch between 7-segment display characters.
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity P3_7seg_clk is
  port (
    clk_in : in std_logic;
    en : in std_logic;
    clk_7seg : out std_logic
  );
end P3_7seg_clk;

architecture rtl of P3_7seg_clk is
  signal counter : unsigned(15 downto 0);
begin
  
  count: process(clk_in, en) is
  begin
    if (en = '0') then
      counter <= (others => '0');
    elsif (rising_edge(clk_in)) then
      counter <= counter + 1;
    end if;
  end process count;

  -- 50% duty cycle
  clk_7seg <= '0' when (counter(15) = '0') else '1';

end rtl;
