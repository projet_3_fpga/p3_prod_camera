----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 06/10/2021 08:02:08 PM
-- Design Name:
-- Module Name: menu_msa - rl
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity menu_msa is
  port (
    -- Entrées
    buttons : in std_logic_vector(3 downto 0);
    reg_hue : in std_logic_vector(9 downto 0);
    reg_sat : in std_logic_vector(8 downto 0);
    reg_val : in std_logic_vector(8 downto 0);
    timer_dn : in std_logic;

    -- Sorties
    disp : out std_logic_vector(9 downto 0);
    reg_select : out std_logic_vector(2 downto 0);
    reg_wr : out std_logic;
    reg : out std_logic_vector(9 downto 0);

    blink_en : out std_logic;
    timer_en : out std_logic;

    -- Misc.
    rst : in std_logic;
    clk : in std_logic
  );
end menu_msa;

architecture rtl of menu_msa is
  -- States:
  --  start: first cycle
  --  select: choose among HUE, SAT, LIGT
  --  set_reg, incr_reg, decr_ref: adjust correction value
  --  confirm: blink display to provide user feedback to confirm the setting
  --           of a number
  --  wait_release_*: wait for all buttons to be released before moving on to
  --                  the next state
  type states is (start,
                  select_hue, select_sat, select_val,
                  wait_release_select_hue, wait_release_select_sat, wait_release_select_val,
                  set_reg,
                  wait_release_set_reg,
                  incr_reg, decr_reg,
                  confirm,
                  wait_release_confirm
                  );

  signal state_p : states;
  signal state_f : states;

  -- Sorties registrées
  signal disp_f : std_logic_vector(9 downto 0);
  signal disp_p : std_logic_vector(9 downto 0);

  signal reg_select_f : std_logic_vector(2 downto 0);
  signal reg_select_p : std_logic_vector(2 downto 0);

  signal reg_wr_f : std_logic;
  signal reg_wr_p : std_logic;

  signal reg_f : signed(9 downto 0);
  signal reg_p : signed(9 downto 0);

  signal blink_en_f : std_logic;
  signal blink_en_p : std_logic;

  signal timer_en_f : std_logic;
  signal timer_en_p : std_logic;

  -- Constantes d'affichage 7-segments
  constant STR_HUE : std_logic_vector(9 downto 0)   := "1111111110";
  constant STR_SAT : std_logic_vector(9 downto 0)   := "1111111101";
  constant STR_VAL : std_logic_vector(9 downto 0)   := "1111111100";
  constant STR_ErrC : std_logic_vector(9 downto 0)  := "1111111011";
  constant STR_BLANK : std_logic_vector(9 downto 0) := "1111111010";

  -- Représentation d'affichage du contenu du registre
  signal reg_str : std_logic_vector(9 downto 0);
  --constant NUM_TO_STR_OFFSET : signed(9 downto 0) := to_signed(390, disp'length);

  alias BTNL is buttons(0);
  alias BTND is buttons(1);
  alias BTNR is buttons(2);
  alias BTNU is buttons(3);
  constant BTNS_ALL_RELEASED : std_logic_vector(3 downto 0) := "0000";

  -- Sélection du registre à écrire
  constant REG_SELECT_HUE  : std_logic_vector(2 downto 0) := "100";
  constant REG_SELECT_SAT  : std_logic_vector(2 downto 0) := "010";
  constant REG_SELECT_VAL  : std_logic_vector(2 downto 0) := "001";
  constant REG_SELECT_NONE : std_logic_vector(2 downto 0) := "000";

  -- Incrément/Décrément du registre à chaque appui de bouton
  constant INCR_PER_PRESS : integer := 5;
begin

ifl: process (all) is
begin
  case state_p is
    when start =>
      state_f <= select_hue;

    when wait_release_select_hue =>
      if (buttons = BTNS_ALL_RELEASED) then state_f <= select_hue;
      else state_f <= wait_release_select_hue;
      end if;

    when select_hue =>
      if (BTND = '1') then state_f <= wait_release_select_sat;
      elsif (BTNR = '1') then state_f <= wait_release_set_reg;
      else state_f <= select_hue;
      end if;

    when wait_release_select_sat =>
      if (buttons = BTNS_ALL_RELEASED) then state_f <= select_sat;
      else state_f <= wait_release_select_sat;
      end if;

    when select_sat =>
      if (BTNU = '1') then state_f <= wait_release_select_hue;
      elsif (BTND = '1') then state_f <= wait_release_select_val;
      elsif (BTNR = '1') then state_f <= wait_release_set_reg;
      else state_f <= select_sat;
      end if;

    when wait_release_select_val =>
      if (buttons = BTNS_ALL_RELEASED) then state_f <= select_val;
      else state_f <= wait_release_select_val;
      end if;

    when select_val =>
      if (BTNU = '1') then state_f <= wait_release_select_sat;
      elsif (BTNR = '1') then state_f <= wait_release_set_reg;
      else state_f <= select_val;
      end if;

    when wait_release_set_reg =>
      if (buttons = BTNS_ALL_RELEASED) then state_f <= set_reg;
      else state_f <= wait_release_set_reg;
      end if;

    when set_reg =>
      if (BTNU = '1') then state_f <= incr_reg;
      elsif (BTND = '1') then state_f <= decr_reg;
      elsif (BTNR = '1') then state_f <= wait_release_confirm;
      elsif (BTNL = '1') then state_f <= wait_release_select_hue;
      else state_f <= set_reg;
      end if;

    when decr_reg =>
      state_f <= wait_release_set_reg;

    when incr_reg =>
      state_f <= wait_release_set_reg;

    when wait_release_confirm =>
      if (buttons = BTNS_ALL_RELEASED) then state_f <= confirm;
      else state_f <= wait_release_confirm;
      end if;

    when confirm => -- wait for a given time to confirm the register write
      if (timer_dn = '1') then state_f <= wait_release_set_reg;
      else state_f <= confirm;
      end if;

    end case;

end process ifl;

reg_str <= std_logic_vector(reg_p + to_signed(390 + 237, reg_p'length)); -- address of the first number in ROM

ofl: process (all) is
begin
  case state_f is
    when start =>
      disp_f <= STR_BLANK;
      reg_select_f <= REG_SELECT_NONE;
      reg_wr_f <= '0';
      reg_f <= (others => '0');
      blink_en_f <= '0';
      timer_en_f <= '0';

    when wait_release_select_hue =>
      disp_f <= STR_BLANK;
      reg_select_f <= REG_SELECT_NONE;
      reg_wr_f <= '0';
      reg_f <= reg_p;
      blink_en_f <= '0';
      timer_en_f <= '0';

    when select_hue =>
      disp_f <= STR_HUE;
      reg_select_f <= REG_SELECT_HUE;
      reg_wr_f <= '0';
      reg_f <= signed(reg_hue);
      blink_en_f <= '0';
      timer_en_f <= '0';

    when wait_release_select_sat =>
      disp_f <= STR_BLANK;
      reg_select_f <= REG_SELECT_NONE;
      reg_wr_f <= '0';
      reg_f <= reg_p;
      blink_en_f <= '0';
      timer_en_f <= '0';

    when select_sat =>
      disp_f <= STR_SAT;
      reg_select_f <= REG_SELECT_SAT;
      reg_wr_f <= '0';
      reg_f <= signed('0' & reg_sat);
      blink_en_f <= '0';
      timer_en_f <= '0';

    when wait_release_select_val =>
      disp_f <= STR_BLANK;
      reg_select_f <= REG_SELECT_NONE;
      reg_wr_f <= '0';
      reg_f <= reg_p;
      blink_en_f <= '0';
      timer_en_f <= '0';

    when select_val =>
      disp_f <= STR_VAL;
      reg_select_f <= REG_SELECT_VAL;
      reg_wr_f <= '0';
      reg_f <= signed('0' & reg_val);
      blink_en_f <= '0';
      timer_en_f <= '0';

    when wait_release_set_reg =>
      disp_f <= STR_BLANK;
      reg_select_f <= reg_select_p;
      reg_wr_f <= '0';
      reg_f <= reg_p;
      blink_en_f <= '0';
      timer_en_f <= '0';

    when set_reg =>
      disp_f <= reg_str;
      reg_select_f <= reg_select_p;
      reg_wr_f <= '0';
      reg_f <= reg_p;
      blink_en_f <= '0';
      timer_en_f <= '0';

    when decr_reg =>
      disp_f <= reg_str;
      reg_select_f <= reg_select_p;
      reg_wr_f <= '0';

      -- Valeur min: -390
      if (reg_p > -390) then
        reg_f <= reg_p - to_signed(INCR_PER_PRESS, reg_f'length);
      else
        reg_f <= reg_p;
      end if;

      blink_en_f <= '0';
      timer_en_f <= '0';

    when incr_reg =>
      disp_f <= reg_str;
      reg_select_f <= reg_select_p;
      reg_wr_f <= '0';

      -- Valeur max: 390
      if (reg_p < 390) then
        reg_f <= reg_p + to_signed(INCR_PER_PRESS, reg_f'length);
      else
        reg_f <= reg_p;
      end if;

      blink_en_f <= '0';
      timer_en_f <= '0';

    when wait_release_confirm =>
      disp_f <= reg_str;
      reg_select_f <= reg_select_p;
      reg_wr_f <= '0';
      reg_f <= reg_p;
      blink_en_f <= '0';
      timer_en_f <= '0';

    when confirm =>
      disp_f <= reg_str;
      reg_select_f <= reg_select_p;
      reg_wr_f <= '1';
      reg_f <= reg_p;
      blink_en_f <= '1';
      timer_en_f <= '1';
    end case;

end process ofl;

registered: process (clk, rst) is
begin
  if rising_edge(clk) then
    if rst = '0' then
      disp_p <= disp_f;
      reg_select_p <= reg_select_f;
      reg_wr_p <= reg_wr_f;
      reg_p <= reg_f;

      blink_en_p <= blink_en_f;
      timer_en_p <= timer_en_f;

      state_p <= state_f;
    else
      disp_p <= STR_BLANK;
      reg_select_p <= REG_SELECT_NONE;
      reg_wr_p <= '0';
      reg_p <= (others => '0');

      blink_en_p <= '0';
      timer_en_p <= '0';

      state_p <= start;
    end if;
  end if;
end process registered;

disp <= disp_p;
reg_select <= reg_select_p;
reg_wr <= reg_wr_p;
reg <= std_logic_vector(reg_p);
blink_en <= blink_en_p;
timer_en <= timer_en_p;

end rtl;
