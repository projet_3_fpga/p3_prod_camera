----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/12/2021 12:32:43 PM
-- Design Name: 
-- Module Name: i2c_master - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity i2c_master is
    generic (
      I2C_SPEED_HZ : integer := 400_000 -- 100kHz or 400kHz preferred
    );
    port (
      sda_i : in STD_LOGIC;
      sda_o : out STD_LOGIC;
      scl : out STD_LOGIC;

      i2c_go : in STD_LOGIC;
      i2c_dn : out STD_LOGIC;

      slave_addr : in STD_LOGIC_VECTOR (6 downto 0);
      r_w : in STD_LOGIC;

      reg_addr : in STD_LOGIC_VECTOR (15 downto 0);
      data_wr : in STD_LOGIC_VECTOR (7 downto 0);
      data_rd : out STD_LOGIC_VECTOR (7 downto 0);

      clk_100MHz : in STD_LOGIC;
      rst : in STD_LOGIC
    );
end i2c_master;

architecture rtl of i2c_master is
  signal i2c_clk : std_logic;
  signal sda_o_early : std_logic;
begin
  -- Instanciation MSA
  i2c_master_msa_inst: entity work.i2c_master_msa
    port map (
      sda_i => sda_i,
      sda_o => sda_o_early,
      scl   => scl,

      i2c_go => i2c_go,
      i2c_dn => i2c_dn,

      slave_addr => slave_addr,
      r_w => r_w,

      reg_addr => reg_addr,
      data_wr => data_wr,
      data_rd => data_rd,

      clk => i2c_clk,
      rst => rst
    );

  -- Délai de 0.2μs sur SDA
  -- ajout un écart entre SCL bas et SDA pour éviter
  -- la détection d'une condition de fin
  sda_delay: entity work.delay_signal
    generic map (
      CLK_FREQUENCY_HZ => 100_000_000,
      DELAY_NS => 200
    )
    port map (
      input   => sda_o_early,
      delayed => sda_o,

      clk => clk_100MHz,
      rst => rst
    );

  -- Diviseur d'horloge 100 MHz -> horloge I2C
  i2c_master_clk_inst: entity work.i2c_master_clk
    generic map (
      CLK_SPEED_OUT_HZ => 2*I2C_SPEED_HZ
    )
    port map (
      clk_in => clk_100MHz,
      clk_out => i2c_clk,
      rst => rst
    );
end rtl;
