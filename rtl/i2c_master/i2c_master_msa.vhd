----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 07/12/2021 02:47:22 PM
-- Design Name:
-- Module Name: i2c_master_msa - rtl
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity i2c_master_msa is
  port (
    sda_i : in STD_LOGIC;  -- open-drain bidirectional port to be implemented
    sda_o : out STD_LOGIC; -- at instantiation
    scl   : out STD_LOGIC;

    i2c_go : in STD_LOGIC; -- run a I2C transaction. Latches in data and addresses
    i2c_dn : out STD_LOGIC; -- I2C transaction done. Data read may be retrieved.

    slave_addr : in STD_LOGIC_VECTOR (6 downto 0); -- Slave device address
    r_w : in STD_LOGIC; -- read => '0', write => '1'

    reg_addr :  in  STD_LOGIC_VECTOR (15 downto 0); -- Register to be accessed
    data_wr :   in  STD_LOGIC_VECTOR (7 downto 0); -- Data to be written from peripheral
    data_rd :   out STD_LOGIC_VECTOR (7 downto 0); -- Data read from peripheral

    clk : in STD_LOGIC; -- 100kHz or 400kHz preferred
    rst : in STD_LOGIC
  );
end i2c_master_msa;

architecture rtl of i2c_master_msa is
  -- États
  type state is ( start,
                  wait_i2c_go,
                  start_condition_d,
                  start_condition_c,
                  write_slave_addr_clk_down,
                  write_slave_addr_clk_up,
                  write_slave_addr_dont_care_down,
                  write_slave_addr_dont_care_up,
                  write_reg_addr_clk_down,
                  write_reg_addr_clk_up,
                  write_reg_addr_dont_care_down,
                  write_reg_addr_dont_care_up,
                  read_data_clk_down,
                  read_data_clk_up,
                  read_data_nack_down,
                  read_data_nack_up,
                  write_data_clk_down,
                  write_data_clk_up,
                  write_data_dont_care_down,
                  write_data_dont_care_up,
                  stop_condition_down,
                  stop_condition_scl_up,
                  stop_condition_sda_up
                );
  signal state_f : state;
  signal state_p : state;

  -- Entrées enregistrées au début de la transaction
  signal slave_addr_rw_f : std_logic_vector(7 downto 0);
  signal slave_addr_rw_p : std_logic_vector(7 downto 0);

  signal r_w_f : std_logic;
  signal r_w_p : std_logic;

  signal reg_addr_f      : std_logic_vector(15 downto 0);
  signal reg_addr_p      : std_logic_vector(15 downto 0);

  signal data_wr_f       : std_logic_vector(7 downto 0);
  signal data_wr_p       : std_logic_vector(7 downto 0);

  -- Compteur
  signal count_f : integer range 0 to 16;
  signal count_p : integer range 0 to 16;

  -- Signaux p et f des sorties
  signal sda_o_f : std_logic;
  signal sda_o_p : std_logic;

  signal scl_f : std_logic;
  signal scl_p : std_logic;

  signal i2c_dn_f  : std_logic;
  signal i2c_dn_p  : std_logic;

  signal data_rd_f : std_logic_vector(7 downto 0);
  signal data_rd_p : std_logic_vector(7 downto 0);

  -- Constantes
  constant R_W_READ   : std_logic := '1';
  constant R_W_WRITE  : std_logic := '0';

begin
  -- IFL
  ifl: process (all) is
  begin
    case state_p is
      -- Wait for transaction start
      when start =>
        state_f <= wait_i2c_go;

      when wait_i2c_go =>
        if (i2c_go = '1') then state_f <= start_condition_d;
        else state_f <= wait_i2c_go;
        end if;

      -- Generate start condition
      when start_condition_d =>
        state_f <= start_condition_c;

      when start_condition_c =>
        state_f <= write_slave_addr_clk_down;

      -- Write slave device address & r/w bit
      when write_slave_addr_clk_down =>
        state_f <= write_slave_addr_clk_up;

      when write_slave_addr_clk_up =>
        if (count_p = 0) then state_f <= write_slave_addr_dont_care_down;
        else state_f <= write_slave_addr_clk_down;
        end if;

      when write_slave_addr_dont_care_down =>
        state_f <= write_slave_addr_dont_care_up;

      when write_slave_addr_dont_care_up =>
        state_f <= write_reg_addr_clk_down;

      -- Write slave register address
      when write_reg_addr_clk_down =>
        state_f <= write_reg_addr_clk_up;

      when write_reg_addr_clk_up =>
        if (count_p = 0) then state_f <= write_reg_addr_dont_care_down;
        else state_f <= write_reg_addr_clk_down;
        end if;

      when write_reg_addr_dont_care_down =>
        state_f <= write_reg_addr_dont_care_up;

      when write_reg_addr_dont_care_up =>
        if (r_w_p = R_W_READ) then state_f <= read_data_clk_down;
        else state_f <= write_data_clk_down;
        end if;

      -- Read data off slave
      when read_data_clk_down =>
        state_f <= read_data_clk_up;

      when read_data_clk_up =>
        if (count_p = 0) then state_f <= read_data_nack_down;
        else state_f <= read_data_clk_down;
        end if;

      when read_data_nack_down =>
        state_f <= read_data_nack_up;

      when read_data_nack_up =>
        state_f <= stop_condition_down;

      -- Write data to slave
      when write_data_clk_down =>
        state_f <= write_data_clk_up;

      when write_data_clk_up =>
        if (count_p = 0) then state_f <= write_data_dont_care_down;
        else state_f <= write_data_clk_down;
        end if;

      when write_data_dont_care_down =>
        state_f <= write_data_dont_care_up;

      when write_data_dont_care_up =>
        state_f <= stop_condition_down;

      -- Generate stop condition
      when stop_condition_down =>
        state_f <= stop_condition_scl_up;

      when stop_condition_scl_up =>
        state_f <= stop_condition_sda_up;

      when stop_condition_sda_up =>
        state_f <= wait_i2c_go;
    end case;
  end process ifl;

  -- OFL
  ofl: process (all) is
  begin
    case state_f is
      when start =>
        sda_o_f   <= '1';
        scl_f     <= '1';
        i2c_dn_f  <= '0';
        data_rd_f <= (others => '0');

        slave_addr_rw_f <= (others => '0');
        r_w_f <= '0';
        reg_addr_f      <= (others => '0');
        data_wr_f       <= (others => '0');

        count_f <= 0;

      when wait_i2c_go =>
        sda_o_f   <= '1';
        scl_f     <= '1';
        i2c_dn_f  <= '1';
        data_rd_f <= data_rd_p;

        slave_addr_rw_f <= (others => '0');
        r_w_f <= '0';
        reg_addr_f      <= (others => '0');
        data_wr_f       <= (others => '0');

        count_f <= 0;

      when start_condition_d =>
        sda_o_f   <= '0';
        scl_f     <= '1';
        i2c_dn_f  <= '0';
        data_rd_f <= (others => '0');

        -- latch in inputs
        slave_addr_rw_f <= slave_addr & r_w;
        r_w_f <= r_w;
        reg_addr_f      <= reg_addr;
        data_wr_f       <= data_wr;

        count_f <= 8;

      when start_condition_c =>
        sda_o_f   <= '0';
        scl_f     <= '0';
        i2c_dn_f  <= '0';
        data_rd_f <= (others => '0');

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= 8;

      when write_slave_addr_clk_down =>
        sda_o_f   <= slave_addr_rw_p(7);
        scl_f     <= '0';
        i2c_dn_f  <= '0';
        data_rd_f <= (others => '0');

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= count_p - 1;

      when write_slave_addr_clk_up =>
        sda_o_f   <= slave_addr_rw_p(7);
        r_w_f <= r_w_p;
        scl_f     <= '1';
        i2c_dn_f  <= '0';
        data_rd_f <= (others => '0');

        slave_addr_rw_f <= slave_addr_rw_p(6 downto 0) & '0';
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= count_p;

      when write_slave_addr_dont_care_down =>
        sda_o_f   <= '1';
        scl_f     <= '0';
        i2c_dn_f  <= '0';
        data_rd_f <= (others => '0');

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= 16;

      when write_slave_addr_dont_care_up =>
        sda_o_f   <= '1';
        scl_f     <= '1';
        i2c_dn_f  <= '0';
        data_rd_f <= (others => '0');

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= 16;

      when write_reg_addr_clk_down =>
        sda_o_f   <= reg_addr_p(15);
        scl_f     <= '0';
        i2c_dn_f  <= '0';
        data_rd_f <= (others => '0');

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= count_p - 1;

      when write_reg_addr_clk_up =>
        sda_o_f   <= reg_addr_p(15);
        scl_f     <= '1';
        i2c_dn_f  <= '0';
        data_rd_f <= (others => '0');

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p(14 downto 0) & '0';
        data_wr_f       <= data_wr_p;

        count_f <= count_p;

      when write_reg_addr_dont_care_down =>
        sda_o_f   <= '1';
        scl_f     <= '0';
        i2c_dn_f  <= '0';
        data_rd_f <= (others => '0');

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= 8;

      when write_reg_addr_dont_care_up =>
        sda_o_f   <= '1';
        scl_f     <= '1';
        i2c_dn_f  <= '0';
        data_rd_f <= (others => '0');

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= 8;

      when read_data_clk_down =>
        sda_o_f   <= '1';
        scl_f     <= '0';
        i2c_dn_f  <= '0';
        data_rd_f <= data_rd_p;

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= count_p - 1;

      when read_data_clk_up =>
        sda_o_f   <= '1';
        scl_f     <= '1';
        i2c_dn_f  <= '0';
        data_rd_f <= data_rd_p(6 downto 0) & sda_i;

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= count_p;

      when read_data_nack_down =>
        sda_o_f   <= '0';
        scl_f     <= '0';
        i2c_dn_f  <= '0';
        data_rd_f <= data_rd_p;

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= count_p;

      when read_data_nack_up =>
        sda_o_f   <= '0';
        scl_f     <= '1';
        i2c_dn_f  <= '0';
        data_rd_f <= data_rd_p;

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= count_p;

      when write_data_clk_down =>
        sda_o_f   <= data_wr_p(7);
        scl_f     <= '0';
        i2c_dn_f  <= '0';
        data_rd_f <= (others => '0');

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= count_p - 1;

      when write_data_clk_up =>
        sda_o_f   <= data_wr_p(7);
        scl_f     <= '1';
        i2c_dn_f  <= '0';
        data_rd_f <= (others => '0');

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p(6 downto 0) & '0';

        count_f <= count_p;

      when write_data_dont_care_down =>
        sda_o_f   <= '1';
        scl_f     <= '0';
        i2c_dn_f  <= '0';
        data_rd_f <= data_rd_p;

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= count_p;

      when write_data_dont_care_up =>
        sda_o_f   <= '1';
        scl_f     <= '1';
        i2c_dn_f  <= '0';
        data_rd_f <= data_rd_p;

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= count_p;

      when stop_condition_down =>
        sda_o_f   <= '0';
        scl_f     <= '0';
        i2c_dn_f  <= '0';
        data_rd_f <= data_rd_p;

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= 0;

      when stop_condition_scl_up =>
        sda_o_f   <= '0';
        scl_f     <= '1';
        i2c_dn_f  <= '1';
        data_rd_f <= data_rd_p;

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= 0;

      when stop_condition_sda_up =>
        sda_o_f   <= '1';
        scl_f     <= '1';
        i2c_dn_f  <= '1';
        data_rd_f <= data_rd_p;

        slave_addr_rw_f <= slave_addr_rw_p;
        r_w_f <= r_w_p;
        reg_addr_f      <= reg_addr_p;
        data_wr_f       <= data_wr_p;

        count_f <= 0;
        
    end case;
  end process ofl;

  -- Processus registré
  registered: process (clk, rst) is
  begin
    if (rising_edge(clk)) then
      if rst = '0' then
        state_p <= state_f;

        sda_o_p <= sda_o_f;
        scl_p <= scl_f;
        i2c_dn_p <= i2c_dn_f;
        data_rd_p <= data_rd_f;

        slave_addr_rw_p <= slave_addr_rw_f;
        r_w_p <= r_w_f;
        reg_addr_p      <= reg_addr_f;
        data_wr_p       <= data_wr_f;

        count_p <= count_f;

      else
        state_p <= start;

        sda_o_p <= '1'; -- Z en O/C
        scl_p <= '1'; -- Z en O/C
        i2c_dn_p <= '0';
        data_rd_p <= (others => '0');

        slave_addr_rw_p <= (others => '0');
        r_w_p <= '0';
        reg_addr_p      <= (others => '0');
        data_wr_p       <= (others => '0');

        count_p <= 0;

      end if;
    end if;
  end process registered;

  -- Placer les sorties
  sda_o <= sda_o_p;
  scl <= scl_p;
  i2c_dn <= i2c_dn_p;
  data_rd <= data_rd_p;

end rtl;
