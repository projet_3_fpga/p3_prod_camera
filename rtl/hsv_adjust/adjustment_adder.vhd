----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/04/2021 06:16:39 PM
-- Design Name: 
-- Module Name: adjustment_adder - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity adjustment_adder is
  port (
    signal hue_in : in std_logic_vector(8 downto 0);
    signal sat_in : in std_logic_vector(7 downto 0);
    signal val_in : in std_logic_vector(7 downto 0);

    signal hue_add : in std_logic_vector(9 downto 0); -- signed
    signal sat_add : in std_logic_vector(8 downto 0); -- signed
    signal val_add : in std_logic_vector(8 downto 0); -- signed
    
    signal hue_out : out std_logic_vector(8 downto 0);
    signal sat_out : out std_logic_vector(7 downto 0);
    signal val_out : out std_logic_vector(7 downto 0);

    signal clk : in std_logic;
    signal rst : in std_logic
  );
end adjustment_adder;

architecture rtl of adjustment_adder is
  -- valeur limites
  constant MAX_HUE : integer := 383;
  constant MAX_SAT : integer := 255;
  constant MAX_VAL : integer := 255;
  
  signal hue_unbound_addition : std_logic_vector(10 downto 0);
  signal sat_unbound_addition : std_logic_vector(9 downto 0);
  signal val_unbound_addition : std_logic_vector(9 downto 0);

begin
  -- Reset synchrone
  add: process(clk, rst)
  begin
    if rising_edge(clk) then    
      
      if rst = '0' then
        -- Cycle 1: additions
        -- FIXME: CARRY ISSUES DAMMIT
        hue_unbound_addition <= std_logic_vector(signed("00" & hue_in) + signed(hue_add));
        sat_unbound_addition <= std_logic_vector(signed("00" & sat_in) + signed(sat_add));
        val_unbound_addition <= std_logic_vector(signed("00" & val_in) + signed(val_add));

        -- Cycle 2: limiter les sortie
        if (signed(hue_unbound_addition) < 0 ) then
          hue_out <= std_logic_vector(to_unsigned(0, hue_out'length));
        elsif (signed(hue_unbound_addition) > to_signed(MAX_HUE, hue_unbound_addition'length)) then
          hue_out <= std_logic_vector(to_unsigned(MAX_HUE, hue_out'length));
        else
          hue_out <= hue_unbound_addition(8 downto 0);
        end if;

        if (signed(sat_unbound_addition) < 0 ) then
          sat_out <= std_logic_vector(to_unsigned(0, sat_out'length));
        elsif (signed(sat_unbound_addition) > to_signed(MAX_SAT, sat_unbound_addition'length)) then
          sat_out <= std_logic_vector(to_unsigned(MAX_SAT, sat_out'length));
        else
          sat_out <= sat_unbound_addition(7 downto 0);
        end if;

        if (signed(val_unbound_addition) < 0 ) then
          val_out <= std_logic_vector(to_unsigned(0, val_out'length));
        elsif (signed(val_unbound_addition) > to_signed(MAX_VAL, val_unbound_addition'length)) then
          val_out <= std_logic_vector(to_unsigned(MAX_VAL, val_out'length));
        else
          val_out <= val_unbound_addition(7 downto 0);
        end if;
      else
        hue_unbound_addition <= (others => '0');
        sat_unbound_addition <= (others => '0');
        val_unbound_addition <= (others => '0');

        hue_out <= (others => '0');
        sat_out <= (others => '0');
        val_out <= (others => '0');
      end if;
    end if;
  end process add;
end rtl;
