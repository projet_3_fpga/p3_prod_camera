#!/usr/bin/python3
# Générer une LUT associant une chaîne à sa représentation 7-segments
# Date: 30-juillet-2021

import os
import math
import datetime
from mako.template import Template

# Config
numbers_depth = 8; # size of ROM
inverse_depth = 16; # inverse precision
generate_path = os.path.join("..", "rtl")
generated_filename = "rom_inverse_" + str(numbers_depth) + "bits_" + str(inverse_depth)\
                    + "depth"

# @brief                 Fonction d'inversion
# @param number          Entier à inverser
# @param depth           Largeur de l'entier et de son inverse en bits
# @param fixed_point_pos Position du pt décimal
#                        0: left, number is all decimal
# @returns inverse       Chaîne de chiffres binaires, avec point decimal avant
#                        le premier caractère
def inverse(number, depth):
    if (number == 0):
      return "0" * depth
    if (number == 1):
      # Precision loss 1 LSB
      return "1" * depth

    max = pow(2, depth)
    if (number > max) or (number < 0):
      print("Number out of bounds " + str(number))
      exit(1)
    inverse = 1/number;
    inverse = round(max*inverse)

    return bin(inverse)[2:].rjust(depth, '0')

# Generate ROM content
linecount = pow(2, numbers_depth)
bottom_address = 0

entry = []
entry_human_readable = []
for i in range(0, linecount):
    entry.append(inverse(i, inverse_depth))
    entry_human_readable.append("2^" + str(inverse_depth) + " * 1 / " + str(i))

# Affichage du VHDL
def format_ROM_entry(entry, address, entry_human_readable, last=False):
    tab = " "*4
    if not last:
      formatted = tab + '"' + entry + '",' + "    -- " + hex(address).ljust(10)\
                  + " \""     + entry_human_readable + "\"\n"
    else:
      # Same without the endline comma
      formatted = tab + '"' + entry + '" ' + "    -- " + hex(address).ljust(10)\
                  + " \""     + entry_human_readable + "\""
    return formatted

rom_content = '';
for i in reversed(range(bottom_address+1, linecount)):
    rom_content += format_ROM_entry(entry[i], i, entry_human_readable[i])

if (bottom_address == 0):
    rom_content += format_ROM_entry(entry[0], 0, entry_human_readable[0], last=True)
else:
    rom_content += format_ROM_entry(entry[bottom_address], bottom_address, \
                  entry_human_readable[bottom_address], last=False)
    for i in reversed(range(1, bottom_address)):
        # Fill rest of ROM with unused addresses
        rom_content += format_ROM_entry(inverse_depth*'0', i, "UNUSED", last=False)
    rom_content += format_ROM_entry(inverse_depth*'0', i, "UNUSED", last=True)

# Debug
#print(rom_content)
#exit(0)

# Fill the ROM Mako template
rom_template = Template(filename='ROM_TEMPLATE.txt')
vhdl_code = rom_template.render(
    datetime = datetime.datetime.now(),
    rom_name = generated_filename,
    description = "Inverses des entiers " + str(numbers_depth) + \
                  "bits, avec précision " + str(inverse_depth) + "bits",
    width_minus_one = str(numbers_depth-1),
    depth_minus_one = str(inverse_depth-1),
    highest_addr = str(linecount-1),
    content = rom_content
)

f = open(os.path.join(generate_path, generated_filename + ".vhdl"), 'w')
f.write(vhdl_code)
f.close()

print(generated_filename + " written to disk.")

exit(0)
